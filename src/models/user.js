const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
const Schema = mongoose.Schema;
const UserSchema = new Schema({
  _id: {
    type: String,
    required: true,
  },
  // Not useful, should be refreshed from discord not stored
  username: {
    type: String,
  },
  avatar: {
    type: String,
  },
  joinDate: {
    type: Date,
    default: new Date(),
  },
  level: {
    type: Number,
    default: 1,
  },
  xp: {
    type: Number,
    default: 0,
  },
  sounds: [{
    type: Schema.Types.ObjectId,
    ref: 'Sound',
  }],
  soundPlays: {
    type: Number,
    default: 0,
  },
  votes: [{
    type: Schema.Types.ObjectId,
    ref: 'Vote',
    default: [],
  }],
});

class UserClass {
  static async findOrCreate(profile) {
    let user = await this.findOne({ _id: profile.id });
    if (!user) {
      user = new this();
      user._id = profile.id;
      user.username = profile.username;
      user.avatar = profile.avatar;
      return user.save();
    }
    return user;
  }
  async giveXp(xp) {
    this.xp += xp;
    return this.save();
  }
}

UserSchema.loadClass(UserClass);

module.exports = mongoose.model('User', UserSchema);

