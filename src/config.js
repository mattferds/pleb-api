let config = {};
switch (process.env.NODE_ENV) {
  case 'dev':
    config = {
      env: 'dev',
      db: 'mongodb://localhost/plebBot',
      redis: process.env.REDIS_URL || '127.0.0.1',
      discordToken: process.env.DISCORD_BOT_TOKEN,
      clientId: process.env.DISCORD_CLIENT_ID,
      clientSecret: process.env.DISCORD_CLIENT_SECRET,
      localServer: 'http://localhost:8080', // Link to the web app using the api (redirect url after auth for local dev server)
      callbackUri: 'http://localhost:3000',
      secrets: [process.env.SECRET_KEY_1 || 'secret key', process.env.SECRET_KEY_2 || '🐱💻🐱💻'],
    };
    break;
  case 'prod':
    config = {
      env: 'prod',
      db: `${process.env.MONGO_URL || 'mongodb://localhost'}/plebBot`,
      redis: process.env.REDIS_URL || '127.0.0.1',
      discordToken: process.env.DISCORD_BOT_TOKEN,
      clientId: process.env.DISCORD_CLIENT_ID,
      clientSecret: process.env.DISCORD_CLIENT_SECRET,
      callbackUri: process.env.CALLBACK_URI,
      secrets: [process.env.SECRET_KEY_1 || 'secret key', process.env.SECRET_KEY_2 || '🐱💻🐱💻'],
    };
    break;
  default:
    config = {
      env: 'dev',
      db: 'mongodb://localhost/plebBot',
      redis: process.env.REDIS_URL || '127.0.0.1',
      discordToken: process.env.DISCORD_BOT_TOKEN,
      clientId: process.env.DISCORD_CLIENT_ID,
      clientSecret: process.env.DISCORD_CLIENT_SECRET,
      callbackUri: 'http://localhost:3000',
      secrets: [process.env.SECRET_KEY_1 || 'secret key', process.env.SECRET_KEY_2 || '🐱💻🐱💻'],
    };
    break;
}
module.exports = config;
