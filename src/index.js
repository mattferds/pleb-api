const Koa = require('koa');
const cors = require('kcors');
const mongoose = require('mongoose');
const koajwt = require('koa-jwt');
const jwt = require('jsonwebtoken');
const bodyparser = require('koa-bodyparser');
const passport = require('koa-passport');
const config = require('./config');
const routes = require('./routes');
const User = require('./models/user');
require('./auth.js');

const app = new Koa();
app.experimental = true;
app.proxy = true;
app.keys = config.secrets;

app.use(bodyparser());
app.use(passport.initialize());

// Use native promises
mongoose.Promise = global.Promise;
mongoose.connect(config.db);

console.log(`Current Environment: ${process.env.NODE_ENV}`);
// In development use cross origin (allows web application access)
if (process.env.NODE_ENV !== 'production') { app.use(cors({ credentials: true })); }

app.use(async (ctx, next) => {
  try {
    await next();
  } catch (err) {
    console.log(err);
    ctx.status = err.status || 500;
    ctx.body = err.message;
    ctx.app.emit('error', err, ctx);
  }
});

routes.public.forEach(route => app.use(route));

app.use(koajwt({
  secret: config.secrets[0],
}));

// Decodes and adds the user id from the JWT
app.use(async (ctx, next) => {
  ctx.authHeader = ctx.request.header.authorization.substring(7); // remove Bearer
  ctx.user = await User.findById(jwt.decode(ctx.authHeader, config.secrets[0]).id);
  await next();
});

routes.auth.forEach(route => app.use(route));

app.listen(3000);
