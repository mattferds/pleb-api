const Sound = require('../models/sound');
const Vote = require('../models/vote');

function createUserResponse(user) {
  return {
    _id: user._id,
    username: user.username,
    avatar: user.avatar,
    level: user.level,
    xp: user.xp,
    soundPlays: user.soundPlays,
  };
}
function createSoundResponse(ctx, sound) {
  return {
    _id: sound._id,
    key: sound.key,
    user: createUserResponse(sound.user),
    playCount: sound.playCount,
    date: sound.date,
    points: sound.points,
    vote: sound.votes.find(vote => vote.user === ctx.user._id),
    duration: sound.duration,
  };
}

module.exports = {
  // Returns list of all sounds
  list: async (ctx) => {
    const sounds = await Sound.find({})
      .populate('user').populate('votes').sort({ key: 'asc' });
    if (sounds) {
      ctx.body = sounds.map(s => createSoundResponse(ctx, s));
    } else {
      ctx.status = 401;
    }
  },
  // Returns information about the specified sound
  one: async (ctx, key) => {
    const sound = await Sound.findOne({ key });
    if (sound) {
      ctx.body = sound;
    } else {
      ctx.status = 401;
    }
  },
  removeVote: async (ctx, key) => {
    if (ctx.isAuthenticated()) {
      const sound = await Sound.findOne({ key }).populate('user').populate('votes');
      if (sound) {
        const vote = sound.votes.splice(sound.votes.findIndex(v => v.user === ctx.user._id), 1)[0];
        if (vote) {
          sound.points -= vote.rating;
          sound.votes.findIndex(v => v);
          await sound.save();
          await Vote.remove({ user: ctx.user._id, sound: sound._id });
          ctx.body = createSoundResponse(ctx, sound);
        } else {
          ctx.status = 401;
        }
      }
    }
  },
  // Adds an upvote to the sound
  upvote: async (ctx, key) => {
    if (ctx.isAuthenticated()) {
      const sound = await Sound.findOne({ key }).populate('user').populate('votes');
      if (sound) {
        await Vote.rate(ctx.user, sound, 1);
        ctx.body = createSoundResponse(ctx, sound);
      } else { ctx.status = 404; }
    } else { ctx.status = 401; }
  },
  // Adds a downvote to the sound
  downvote: async (ctx, key) => {
    if (ctx.isAuthenticated()) {
      const sound = await Sound.findOne({ key }).populate('user').populate('votes');
      if (sound) {
        await Vote.rate(ctx.user, sound, -1);
        ctx.body = createSoundResponse(ctx, sound);
      } else { ctx.status = 404; }
    } else { ctx.status = 401; }
  },
};
